const { Schema, Types } = require("mongoose");

let channelSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    is_private: {
        type: Boolean,
        required: true,
        default: false
    },
    users: {
        type: [Types.ObjectId],
        default: [],
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now
    },
    permissions_needed: {
        type: Array,
        default: ["user"]
    },
    server_id: {
        type: Types.ObjectId,
        required: true
    }
});

let messageSchema = new Schema({
    sender_id: {
        type: Types.ObjectId,
        required: true,
    },
    sender_username: {
        type: String,
        required: true,
    },
    message: {
        type: String,
        required: true,
    },
    sended_at: {
        type: Date,
        required: true,
        default: Date.now
    },
    channel_id: {
        type: Types.ObjectId,
        required: true,
    }
});
let serverSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now
    },
    priority: {
        type: Number,
        required: true,
        default: 0
    }
});
let userSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: Array,
        default: ["user"]
    }
});

module.exports = { channelSchema, messageSchema, serverSchema, userSchema}