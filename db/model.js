const { model } = require("mongoose");
const { userSchema, serverSchema, channelSchema, messageSchema} = require("./schema");

let UserModel = model("User", userSchema);
let ServerModel = model('Server', serverSchema);
let ChannelModel = model("Channel", channelSchema);
let MessageModel = model("Message", messageSchema);

module.exports = { 
    ChannelModel, 
    MessageModel, 
    ServerModel, 
    UserModel }