require("dotenv").config();
const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);

const { createHmac } = require('crypto');
const mongoose = require("mongoose");
const cookieParser = require("cookie-parser");
const cors = require("cors");

const { UserModel, ServerModel, ChannelModel, MessageModel } = require("./db/model");

const jwt = require("jsonwebtoken");
// pour le fun
const secret =  createHmac("sha256", "secretKey").update("secretToChangeInProd").digest("base64");

mongoose.connect(process.env.MONGO_URL, {maxPoolSize: 20}, (error) => {
    if(error) {
        throw error;
    }
    ServerModel.updateOne({name: "Messages Privé"}, {name: "Messages Privé", priority: 10}, {upsert: true}, (err, result) => {
      if (err) {throw err;}
      ServerModel.findOneAndUpdate({name: "Serveur 1"}, {name: "Serveur 1", priority: 0}, {upsert: true, new: true}, (err2, result2) => {
        if (err2) { throw err2;}
        ChannelModel.updateOne({name: "General", server_id: result2._id}, {name: "General", server_id: result2._id}, {upsert: true}, (err3, result3) => {
          if (err3) { throw err3;}
          server.listen(8080, () => {
            console.log("Listening on *:8080");
          })
        })
      })
    })
});

app.use(cookieParser());
app.use(express.json());
app.use(cors({
  origin: "http://localhost:8080"
}));

app.use((req, res, next) => {
  if(!req.accepts('json')) {
    return res.status(406).json({ code: 406, message: 'Not Acceptable' });
  }

  if(req.is('json') === false) {
    return res.status(415).json({ code: 415, message: 'Unsupported Media Type' });
  }

  next();
});

app.use(async (req, res, next) => {
  res.locals.user = null;
  if (req.cookies.authorization) {
    try {
      let decoded = jwt.verify(req.cookies.authorization, secret);
      res.locals.user = await UserModel.findOne({username: decoded.sub});
    } catch (err) {
    }
  }
  next();
});

app.use('/css', express.static(__dirname + '/public/css'));

app.get('/', (req, res) => {
  if(!res.locals.user) {
    return res.redirect("/login");
  }

  res.sendFile(__dirname + '/public/index.html');
});

app.get('/signup', (req, res) => {
  if(res.locals.user) {
    return res.redirect("/");
  }

  res.sendFile(__dirname + '/public/signin.html');
});

app.get("/login", (req, res) => {
  if(res.locals.user) {
      return res.redirect("/");
  }
  res.sendFile(__dirname + "/public/login.html");
});

app.get("/logout", (req, res) => {
  res.cookie("authorization", '', {expires: new Date(0)});
  res.redirect("/");
});

app.post("/signup", async (req, res) => {
  const {username, password} = req.body;
  if (username && password) {
    try {
      const newUser = new UserModel({username, password});
      await newUser.save();
    } catch (err) {
      console.log(err);
      return res.status(409).send({
        error : err,
      });
    }

    const token = jwt.sign(
      {sub: username},
      secret,
      { algorithm: 'HS256', expiresIn: 86400 }
    )
    res.cookie("authorization", token, {
      sameSite: "lax",
      httpOnly: true
    });
    //res.redirect("/");
  } else {
    res.status(422).send({
      error : "missing username or password"
    });
  }
});

app.get("/user/me", async (req, res) => {
  if (!res.locals.user) {
    return res.status(404).end();
  }
  let user = await UserModel.findOne({username: res.locals.user.username});
  return res.json(user);
});

app.post("/login/token", async (req, res) => {
  const {username, password, fingerprint} = req.body;
  if (username && password) {
    let user = await UserModel.findOne({username : username, password : password});
    if (!user) {
      // si on se log via l'empreinte digital sur l'application mobile, 
      // alors on creer un compte à la volé avec le nom d'utilisateur et le mot de passe
      if (fingerprint) {
        const newUser = new UserModel({username, password});
        await newUser.save();
      } else {
        return res.status(404).send({
          error: "user not found"
        });
      }
    }

    const token = jwt.sign(
      {sub: username},
      secret,
      { algorithm: 'HS256', expiresIn: 86400 }
    )
    res.cookie("authorization", token, {
      sameSite: "lax",
      httpOnly: true
    });
    //res.redirect("/");
    res.json({token: token});
  } else {
    res.status(422).send({
      error : "missing username or password"
    });
  }
});

app.get("/servers", async (req, res) => {
  let servers = await ServerModel.find({}).sort({priority: -1});
  res.status(200).json(servers);
});

app.post('/servers', async (req, res) => {
  if (!res.locals.user) {
    return res.status(401).send({
      code: 401,
      message: "unauthorized"
    });
  }
  const { name } = req.body;
  if (name) {
    const newServer = new ServerModel({name : name});
    await newServer.save();

    const generalChannel = new ChannelModel({name: "General", server_id: newServer._id});
    await generalChannel.save();
  
    createSocketIoNamespace(newServer);

    res.json(newServer);
  } else {
    res.status(422).send({
      code: 422,
      message: "missing params",
    });
  }
});

app.get("/channels", async (req, res) => {
  const { server, private } = req.query;
  let search = {};
  if (server)
    search["server_id"] = server;
  if (private)
    search["users"] = { $all: [
      { $elemMatch: { $eq: mongoose.Types.ObjectId(res.locals.user._id) }}
    ]};

  let channels = await ChannelModel.find(search);
  res.json(channels);
});

app.post("/channels", async (req, res) => {
  if (!res.locals.user) {
    return res.status(401).send({
      code: 401,
      message: "unauthorized"
    });
  }
  const { name, server_id } = req.body;
  if (name && server_id) {
    const newChannel = new ChannelModel({name, server_id});
    await newChannel.save();
    res.json(newChannel);
  } else {
    res.status(422).send({
      code: 422,
      message: "missing params",
    });
  }
});

let allServer = [];

ServerModel.find((err, servers) => {
  if (err){throw err;}
  servers.forEach((server) => {
    allServer.push(server._id);
    createSocketIoNamespace(server)
  })
});

function createSocketIoNamespace(server) {
  io.of('/' + server._id).use(async (socket, next) => {
    if(socket.handshake.headers.cookie) {
      let cookieString = socket.handshake.headers.cookie;
      let cookieStringSplitted = cookieString.split(';');
      let isAuthCookie = false
      cookieStringSplitted.forEach(async (cookie) => {
        cookie = cookie.trim();
        let cookieKeyValue = cookie.split("=");
        if (cookieKeyValue[0] == "authorization") {
          isAuthCookie = true;
          let token = cookieKeyValue[1];
          try {
            let decoded = jwt.verify(token, secret);
            socket.token = token;
            let userFromDB = await UserModel.findOne({username: decoded.sub}).exec();
            socket.user = userFromDB;
            next();
          } catch(err) {
            let error = new Error("jwt error");
            error.data = { content: "401" };
            next(error);
          }
        }
      })
      if (!isAuthCookie) {
        let err = new Error("not authorized");
        err.data = { content: "401" };
        next(err);
      }
    } else {
      let err = new Error("not authorized");
      err.data = { content: "401" };
      next(err);
    }
  });

  io.of("/" + server._id).on("connection", async (socket) => {
    socket.join(socket.user.username);

    // let server_channels = await ChannelModel.find({server_id: server._id});
    // server_channels.forEach((server_channel) => {
    //   socket.join(server_channel._id);
    // })
    //io.of("/" + server._id).emit('enter', socket.user.username);
    
    socket.on('disconnect', () => {
      if (socket.currentChannel) {
        socket.to(socket.currentChannel).emit("leave", socket.user.username);
      }
    });

    socket.on("join-room", async (channel) => {
      socket.currentChannel = channel;
      socket.join(channel);
      socket.to(channel).emit("enter", socket.user.username);
      const messages = await MessageModel.find({channel_id: channel});
      messages.forEach((message) => {
        socket.emit("message", {user_id: message.sender_id, sended_at : (message.sended_at).toLocaleString('fr-FR'), username: message.sender_username, message: message.message});
      })
    });

    socket.on("private-message", async (data) => {
      if (data) {
        const newChannel = await ChannelModel.findOneAndUpdate(
          {
            is_private: true,
            users: { $all: [
              { $elemMatch: { $eq: mongoose.Types.ObjectId(data.id) }},
              { $elemMatch: { $eq: mongoose.Types.ObjectId(socket.user._id) }}
            ]}
          },
          {
            is_private: true,
            users: [data.id, data.to],
            name: socket.user.username + "-" + data.username,
            server_id: data.server_id
          },
          {
            upsert: true,
            new: true
          }
        )
        socket.emit("new-private", {channel: newChannel});
      }
    });

    socket.on("leave-room", (channel) => {
      socket.leave(channel);
      socket.to(channel).emit("leave", socket.user.username);
    });
  
    socket.on('message', async (data) => {
      const {channel, message} = data;
      if (channel && message) {
        const newMessage = new MessageModel({
          sender_id: socket.user._id,
          sender_username: socket.user.username,
          message: message,
          channel_id: channel,
        });
        await newMessage.save();
        io.of("/" + server._id).to(channel).emit('message', {user_id: socket.user._id, sended_at : (newMessage.sended_at).toLocaleString('fr-FR'), username: socket.user.username, message: newMessage.message});
      }
    });
  })
}

function findSocketByUserIdInAllNamespace(username) {
  allServer.forEach(async (server) => {
    let sockets = await io.of("/" + server).fetchSockets();
    sockets.forEach((socket) => {
      if (socket.user.username == username) {
        return socket;
      }
    })
  })
  return false;
}